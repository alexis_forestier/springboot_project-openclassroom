package com.training.HelloWorld;

import com.training.HelloWorld.service.BusinessService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HelloWorldTerminalApplicationTests {

	@Autowired
	private BusinessService bs;

	@Test
	void contextLoads() {
	}

	@Test
	public void testGetHelloWorld() {
		String expected = "HelloWorld!";
		String result = bs.getHelloWorld().getValue();

		expected.equals(result);
	}

}
