package com.training.HelloWorld.model;

public class HelloWorld {

    private String value;

    public HelloWorld(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "HelloWorld{" +
                "value='" + value + '\'' +
                '}';
    }
}
