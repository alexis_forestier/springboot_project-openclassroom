package com.training.HelloWorld.service;

import com.training.HelloWorld.model.HelloWorld;
import org.springframework.stereotype.Component;

@Component
public class BusinessService {

    public HelloWorld getHelloWorld(){
        return new HelloWorld("HelloWorld!");
    }
}
