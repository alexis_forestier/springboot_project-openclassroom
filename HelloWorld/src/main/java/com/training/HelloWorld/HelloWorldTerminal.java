package com.training.HelloWorld;

import com.training.HelloWorld.model.HelloWorld;
import com.training.HelloWorld.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Qualifier("HelloWorld")
public class HelloWorldTerminal implements CommandLineRunner {

    @Autowired
    private BusinessService bs;

    @Override
    public void run(String... args) throws Exception {
        HelloWorld hw = bs.getHelloWorld();
        System.out.println(hw);
    }
}
