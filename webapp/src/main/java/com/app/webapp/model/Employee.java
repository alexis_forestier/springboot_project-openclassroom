package com.app.webapp.model;

import lombok.Data;

@Data
public class Employee {
    private Integer id;

    private String firstName;

    private String lastName;

    private String mail;

    private String password;

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", mail='" + mail + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
