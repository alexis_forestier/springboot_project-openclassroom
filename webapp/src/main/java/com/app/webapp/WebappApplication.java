package com.app.webapp;

import com.app.webapp.model.Employee;
import com.app.webapp.repository.CustomProperties;
import com.app.webapp.repository.EmployeeProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Iterator;

@SpringBootApplication
public class WebappApplication implements CommandLineRunner {

	@Autowired
	private CustomProperties properties;
	private  EmployeeProxy employeeProxy;

	public static void main(String[] args) {
		SpringApplication.run(WebappApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("API URL : " + properties.getApiUrl());
		System.out.println("Website URL : " + properties.getUrl());
	}
}
