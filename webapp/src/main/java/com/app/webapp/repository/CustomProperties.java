package com.app.webapp.repository;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "com.app.webapp")
@Data
public class CustomProperties {
    private String apiUrl;
    private String url;
}
